package davies.group.automation.task;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class SignInUserValidationClass extends ExecutionFile{
	
	// Assertion mentioned to validate
	
	@SuppressWarnings("unused")
	private WebDriver signinuser;
	
	@FindBy(partialLinkText = "Sign in")
	WebElement Signinbutton;
	
	@FindBy(css="#email_create")
	WebElement newusermailid;
	
	@FindBy(id = "SubmitCreate")
	WebElement createuser;
	
	@FindBy (css = "#id_gender1")
	WebElement gendertitle;
	
	@FindBy (css = "#customer_firstname")
	WebElement userfirstname;
	
	@FindBy (css = "#customer_lastname")
	WebElement userlastname;
	
	@FindBy (css="#passwd")
	WebElement passwordregister;
	
	@FindBy (css="#days")
	WebElement date;
	
	@FindBy (css="#months")
	WebElement month;
	
	@FindBy (css="#years")
	WebElement year;
	
	@FindBy (css="#newsletter")
	WebElement signupnewsletter;
	
	@FindBy (css="#optin")
	WebElement specialoffer;
	
	@FindBy (css="#company")
	WebElement firm;
	
	@FindBy (css="#address1")
	WebElement addressone;
	
	@FindBy (css="#address2")
	WebElement addresstwo;
	
	@FindBy (css="#city")
	WebElement city;
	
	@FindBy (css="#id_state")
	WebElement statetext;
	
	@FindBy (css="#postcode")
	WebElement zipcode;
	
	@FindBy (css="#id_country")
	WebElement countryselect;
	
	@FindBy (css="#other")
	WebElement additionalinformation;
	
	@FindBy (css="#phone")
	WebElement homenumber;
	
	@FindBy (css="#phone_mobile")
	WebElement mobilenumber;
	
	@FindBy (css="#alias")
	WebElement addressfuturereference;
	
	@FindBy (css="#submitAccount")
	WebElement registerbutton;
	
	@FindBy (className ="logout")
	WebElement signoutbutton;
	
	@SuppressWarnings("static-access")
	public SignInUserValidationClass (WebDriver signinuser)
	{
		this.daviesgroup = signinuser;
	}
	
	public void SigninUserValidaionMethod(String mail, String firstname, String lastname, String password, String day, String mnth, String yr, String cmpy,
			String ad1, String ad2, String cty, String statetxt, String zipcde, String cntry, String adinfo, String homenum, String mobilenum, String addref) throws InterruptedException
	{
		// Click on Signin button to start register user
		Signinbutton.click();
		Thread.sleep(2000);
		
		// Enter new user Email ID
		newusermailid.sendKeys(mail);
		Assert.assertTrue(newusermailid.isDisplayed());
		Thread.sleep(2000);
		
		//CLick on Create account button
		createuser.click();
		Thread.sleep(2000);
		
		//Will click on Gender Title
		gendertitle.click();
		Thread.sleep(2000);
		
		//Enter user firstname
		userfirstname.sendKeys(firstname);
		Assert.assertTrue(userfirstname.isDisplayed());
		Thread.sleep(2000);
		
		//Enter user lastname
		userlastname.sendKeys(lastname);
		Assert.assertTrue(userlastname.isDisplayed());
		Thread.sleep(2000);
		
		//Enter password
		passwordregister.sendKeys(password);
		Thread.sleep(2000);
		
		//Select date from dropdown by value
		new Select(date).selectByValue(day);
		Thread.sleep(2000);
		
		//Select month from dropdown by text
		new Select(month).selectByVisibleText(mnth);
		Thread.sleep(2000);
				
		//Select year from dropdown by text
		new Select(year).selectByVisibleText(yr);
		Thread.sleep(2000);
		
		// SignUp News Letter radio button click
		signupnewsletter.click();
		Thread.sleep(2000);
		
		// Special Offer radio button click
		specialoffer.click();
		Thread.sleep(2000);
		
		//Enter firm name
		firm.sendKeys(cmpy);
		Assert.assertTrue(firm.isDisplayed());
		Thread.sleep(2000);
		
		//Enter addressone name
		addressone.sendKeys(ad1);
		Assert.assertTrue(addressone.isDisplayed());
		Thread.sleep(2000);
				
		//Enter addresstwo name
		addresstwo.sendKeys(ad2);
		Assert.assertTrue(addresstwo.isDisplayed());
		Thread.sleep(2000);
				
		//Enter city name
		city.sendKeys(cty);
		Thread.sleep(2000);
		
		//Selct state from dropdown using Text
		new Select(statetext).selectByVisibleText(statetxt);
		Thread.sleep(2000);
		
		//Enter ZipCode name
		zipcode.sendKeys(zipcde);
		Assert.assertTrue(zipcode.isDisplayed());
		Thread.sleep(2000);
		
		//Select country from dropdown using Text 
		new Select(countryselect).selectByVisibleText(cntry);
		Thread.sleep(2000);
		
		//Enter Additional Information
		additionalinformation.sendKeys(adinfo);
		Assert.assertTrue(additionalinformation.isDisplayed());
		Thread.sleep(2000);
		
		//Enter homenumber Text
		homenumber.sendKeys(homenum);
		Assert.assertTrue(homenumber.isDisplayed());
		Thread.sleep(2000);
		
		//Enter mobilenumber Text
		mobilenumber.sendKeys(mobilenum);
		Assert.assertTrue(mobilenumber.isDisplayed());
		Thread.sleep(2000);
		
		//Clear text in the field --> Defect raised for the default text.
		addressfuturereference.clear();
		Thread.sleep(2000);
		
		//Enter Future Address Reference text
		addressfuturereference.sendKeys(addref);
		Assert.assertTrue(addressfuturereference.isDisplayed());
		Thread.sleep(2000);
		
		//Click on Register button
		registerbutton.click();
		Thread.sleep(2000);
		
		//Singout button click
		signoutbutton.click();
		Thread.sleep(2000);
	}

}

