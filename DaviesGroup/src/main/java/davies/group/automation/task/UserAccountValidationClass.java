package davies.group.automation.task;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class UserAccountValidationClass extends ExecutionFile{
	
	// Assertion mentioned to validate 
	
	@SuppressWarnings("unused")
	private WebDriver validateuseraccount;
	
	@FindBy(css = ".login")
	WebElement loginbutton;
	
	@FindBy(id = "email")
	WebElement emailenter;
	
	@FindBy(id = "passwd")
	WebElement passwordenter;
	
	@FindBy(id = "SubmitLogin")
	WebElement signin;
	
	@FindBy (id = "firstname")
	WebElement firstnamevalidate;
	
	@FindBy (id = "lastname")
	WebElement lastnamevalidate;
	
	@FindBy (xpath = ".//span[text()='My personal information']")
	WebElement personalinformationtabclick;
	
	
	@SuppressWarnings("static-access")
	public UserAccountValidationClass (WebDriver validateuseraccount)
	{
		this.daviesgroup = validateuseraccount;
	}
	
	public void UserAccountValidationMethod(String mailtext, String passwordtext) throws InterruptedException
	{
		// Click on Signin button to start register user
		loginbutton.click();
		Thread.sleep(3000);
		
		// Enter registered email ID
		emailenter.sendKeys(mailtext);
		Thread.sleep(3000);
		
		// Enter registered email ID
		passwordenter.sendKeys(passwordtext);
		Thread.sleep(3000);
		
		// Enter registered email ID
		signin.click();
		Thread.sleep(3000);
		
		//Click on Personal Information Tab click to know registered user details
		personalinformationtabclick.click();
		Thread.sleep(3000);
		
		//Validate user firstname text stored
		String firstnametextvalidate = firstnamevalidate.getAttribute("value");
		System.out.println(firstnametextvalidate);
		Assert.assertTrue(firstnamevalidate.isDisplayed());
		Assert.assertEquals(firstnametextvalidate, "Davies");
		
		//Validate user firstname text stored
		String lastnametextvalidate = lastnamevalidate.getAttribute("value");
		System.out.println(lastnametextvalidate);
		Assert.assertTrue(lastnamevalidate.isDisplayed());
		Assert.assertEquals(lastnametextvalidate, "Group");
		
		//Validate user firstname text stored
		String emailentervalidate = emailenter.getAttribute("value");
		System.out.println(emailentervalidate);
		Assert.assertTrue(emailenter.isDisplayed());
		Assert.assertEquals(emailentervalidate, "AutomationTask@daviesgroup.co.uk");
		
	}
}
