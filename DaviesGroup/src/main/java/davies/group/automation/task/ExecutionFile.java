package davies.group.automation.task;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class ExecutionFile {
	
	public static WebDriver daviesgroup = null;
	String url = "http://automationpractice.com/index.php";
	String webapp = "Chrome";
	String userDir = System.getProperty("user.dir");
	
	public static SignInUserValidationClass registeruservalidate;
	public static UserAccountValidationClass useraccountvalidate;
	public static AddItemToCartValidationClass additemtocartvalidate;
	
	@BeforeTest
	public void Setup()
	{
		if (webapp.equalsIgnoreCase("Chrome"))
		{
			System.setProperty("webdriver.chrome.driver", userDir+"\\drivers\\chromedriver.exe");
			daviesgroup = new ChromeDriver();
		}
		
		else if (webapp.equalsIgnoreCase("Firefox"))
		{
			System.setProperty("webdriver.gecko.driver", userDir+"\\drivers\\geckodriver.exe");
			daviesgroup = new FirefoxDriver();
		}
		
		daviesgroup.manage().window().maximize();
		daviesgroup.navigate().to(url);
		
		registeruservalidate = PageFactory.initElements(daviesgroup, SignInUserValidationClass.class);
		useraccountvalidate = PageFactory.initElements(daviesgroup, UserAccountValidationClass.class);
		additemtocartvalidate = PageFactory.initElements(daviesgroup, AddItemToCartValidationClass.class);
	}
	
	@Test(priority=0, enabled=true)
	public void SignInUserValidationTest() throws InterruptedException
	{
		registeruservalidate.SigninUserValidaionMethod("AutomationTask@daviesgroup.co.uk", "Davies", "Group", "Tester99+", "2", "July ", "1989  ", "Davies-Group", "P.O.Box: 397", "Pleasant Valley Rd", "Harrisburg", 
				                                        "Pennsylvania","19328", "United States", "Automation Test","0123456789", "07777777777", "Address for future reference");
	}
	
	@Test(priority=1, enabled=true)
	public void UserAccountValidationTest() throws InterruptedException
	{
		useraccountvalidate.UserAccountValidationMethod("AutomationTask@daviesgroup.co.uk","Tester99+");
	}
	
	@Test(priority=2, enabled=true)
	public void AddItemToCartValidationTest() throws InterruptedException
	{
		additemtocartvalidate.AddItemToCartValidationMethod();
	}
	
	@AfterTest
	public void closewebapp() throws InterruptedException
	{
		daviesgroup.close();
	}

}
