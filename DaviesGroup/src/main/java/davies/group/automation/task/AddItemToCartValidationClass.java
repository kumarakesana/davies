package davies.group.automation.task;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class AddItemToCartValidationClass extends ExecutionFile{
	
	// Assertion mentioned to validate
	
	@SuppressWarnings("unused")
	private WebDriver additemtocart;
	
	@FindBy (xpath = "//*[@id=\"block_top_menu\"]/ul/li[3]/a")
	WebElement tshirtbuttonclick;
	
	@FindBy (css="#color_2")
	WebElement colourpick;
	
	@FindBy (xpath = "//*[@id=\"center_column\"]/div/div/div[3]/h1")
	WebElement selecteditemtext;
	
	@FindBy (xpath = "//*[@id=\"add_to_cart\"]/button/span")
	WebElement addtocartclick;
	
	@FindBy (css=".cross")
	WebElement crossbuttonclick;
	
	@FindBy (xpath = "//*[@id=\"header\"]/div[3]/div/div/div[3]/div/a")
	WebElement shoppingcart;
	
	@FindBy (xpath = "/html/body/div/div[2]/div/div[3]/div/div[2]/table/tbody/tr/td[2]/p/a")
	WebElement validateselecteditemtext;
	
	@FindBy (className ="logout")
	WebElement signoutbutton;
	
	@FindBy (css="#header_logo")
	WebElement logoclick;
	
	@SuppressWarnings("static-access")
	public AddItemToCartValidationClass (WebDriver additemtocart)
	{
		this.daviesgroup = additemtocart;
	}
	
	public void AddItemToCartValidationMethod() throws InterruptedException
	{
		
		// Click on T-Shirt button in order to add item to Cart
		tshirtbuttonclick.click();
		Thread.sleep(3000);
		
		//Picks second colour from the T-Shirt in the list 
		colourpick.click();
		Thread.sleep(3000);
		
		try
		{
		colourpick.click();
		Thread.sleep(3000);
		}
		catch (Exception e)
		{
			
		}
		
		//Get the text of the Selected T-Shirt
		String selecteditemtextvalidate = selecteditemtext.getText();
		System.out.println(selecteditemtextvalidate);
		Assert.assertTrue(selecteditemtext.isDisplayed());
		Thread.sleep(3000);
		
		//Click on Add to Cart for the selected item
		addtocartclick.click();
		Thread.sleep(3000);
		
		//Click on cross button to close layout
		crossbuttonclick.click();
		Thread.sleep(3000);
		
		//Shopping cart button click
		shoppingcart.click();
		Thread.sleep(3000);
		
		//Validate text of the Selected T-Shirt from the Cart
		String selecteditemtextverification = validateselecteditemtext.getText();
		System.out.println(selecteditemtextverification);
		Assert.assertTrue(validateselecteditemtext.isDisplayed());
		Thread.sleep(2000);
		
		//Validate the selected Item from the cart.
		Assert.assertEquals(selecteditemtextverification, selecteditemtextvalidate);
		
		//Singout button click
		signoutbutton.click();
		Thread.sleep(2000);
		
		//Click on Logo in order to navigate back to Homepage
		logoclick.click();
		Thread.sleep(2000);
	}

}
